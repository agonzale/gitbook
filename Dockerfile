# Based on dockerhub fellah/gitbook
#
# We keep our own in gitlab-registry.cern.ch/cloud, just in case.

FROM node:6-slim

MAINTAINER Ricardo Rocha <ricardo.rocha@cern.ch>

ARG VERSION=3.2.0
LABEL version=$VERSION

RUN npm install -g gitbook-cli && \
	gitbook fetch ${VERSION} && \
	npm cache clear && \
        sed -i 's/confirm: true/confirm: false/g' /root/.gitbook/versions/${VERSION}/lib/output/website/copyPluginAssets.js && \
	rm -rf /tmp/*

WORKDIR /srv/gitbook

VOLUME /srv/gitbook /srv/html

EXPOSE 4000 35729

CMD /usr/local/bin/gitbook serve
